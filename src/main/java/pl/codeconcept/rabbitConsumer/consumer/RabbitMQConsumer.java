package pl.codeconcept.rabbitConsumer.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQConsumer {

    @RabbitListener(queues = "teksas1Queue")
    public void recievedMessage1(String message) {
        System.out.println("Recieved Message From RabbitMQ teksas1Queue: " + message);
    }

    @RabbitListener(queues = "teksas2Queue")
    public void recievedMessage2(String message) {
        System.out.println("Recieved Message From RabbitMQ teksas2Queue: " + message);
    }

    @RabbitListener(queues = "teksas3Queue")
    public void recievedMessage3(String message) {
        System.out.println("Recieved Message From RabbitMQ teksas3Queue: " + message);
    }

    @RabbitListener(queues = "allQueue")
    public void recievedMessage4(String message) {
        System.out.println("Recieved Message From RabbitMQ topic allQueue: " + message);
    }

    @RabbitListener(queues = "fanout")
    public void recievedMessage5(String message) {
        System.out.println("Recieved Message From RabbitMQ topic fanout: " + message);
    }
}
